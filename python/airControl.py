import Adafruit_DHT
import json
import time

from configHandler import logger
import configHandler
import heatControl

PIN = 17 # GPIO pin where temperature and humidity sensor is connected

class AirControl:	
	def __init__(self):
		self._sensor = Adafruit_DHT.DHT11	# Sensor type
		self._config = configHandler		# Instance of config interface
		self._read_temp = 0			# Sensor read temperature in CELSIUS degrees
		self._ideal_temp = 0		# Desired temperature in CELSIUS degrees
		self._read_humidity = 0		# Sensor read humidity in % 
		self._ideal_humidity = 0	# Desired humidity in %

		self._air_control = True if configHandler.getConfigHeatingControl()==1 else False
		self._scheduler_control = True if configHandler.getConfigSchedulerState()==1 else False
		self.setupSchedulerTimeFromConfig()

		self._ideal_temp = configHandler.getConfigTemperature()
		self._ideal_humidity = configHandler.getConfigHumidity()

		self.readAirConditions()
		
	def printValues(self):
		print ("*** Air condition values ***")
		print ("Sensor:", self._sensor)
		print ("Pin:", PIN )
		print ("Read T.:", self._read_temp)
		print ("Ideal T.:", self._ideal_temp)
		print ("Read H.:", self._read_humidity)
		print ("Ideal H:", self._ideal_humidity)
		print ("Turn On Heat at: ",str(self._hour_on), ".", str(self._minute_on))
		print ("Turn Off Heat at: ",str(self._hour_off), ".",  str(self._minute_off))
		print ("CfgHandler:", self._config.printValues())

	#### 	Control
	def readAirConditions(self):
		humidity, temperature = Adafruit_DHT.read_retry(self._sensor, PIN)
		if humidity is not None and temperature is not None:
			# fahrenheit = 9.0 / 5.0  * temperature + 32
			self._read_temp = temperature
			self._read_humidity = humidity
	
	def getFormattedHourString(self, hour, minute):
		hour = str(hour)
		minute = str(minute)
		if len(hour) == 1:
			hour = "0" + hour
		if len(minute) == 1:
			minute = "0" + minute
		return  hour + "." + minute

	def getAirControlStatusJSON(self):
		if self._read_humidity is not None and self._read_temp is not None:
		    # fahrenheit = 9.0 / 5.0  * self._read_temp + 32
			return json.dumps({
				"air_control": self._air_control,
				"scheduler_control": self._scheduler_control,
		    	"temperature": self._read_temp,			
		    	"humidity": self._read_humidity,
		    	"perfect_temperature": self._ideal_temp,
		     	"perfect_humidity": self._ideal_humidity,
		    	"air_ctrl_time_on": self.getFormattedHourString(self._hour_on, self._minute_on),
				"air_ctrl_time_off": self.getFormattedHourString(self._hour_off, self._minute_off),
		    })
		else:
		    return False

	def setIdealTemperature(self, celsius):
		self._ideal_temp = celsius
		self._config.saveTemperature(celsius)
		logger.info("Setting new ideal temperature at %s oC" % celsius)
		return self.getAirControlStatusJSON()

	def setIdealHumidity(self, celsius):
		self._ideal_humidity = celsius
		self._config.saveHumidity(celsius)
		logger.info("Setting new ideal humidity at %s percent of RH" % celsius)
		return self.getAirControlStatusJSON()
	
	def watchConditions(self):
		self.readAirConditions()
		readingVsIdealT = str(self._read_temp)+ " oC - " + str(self._ideal_temp) + " oC (IDEAL)"
		readingVsIdealH = str(self._read_humidity)+ " % - " + str(self._ideal_humidity) + " % (IDEAL)"

		if ((self._air_control or self.doesSchedulerAllowsHeathing()) and self._read_temp < self._ideal_temp):	#trigger low temp signal
			logger.debug("Low temp signal " + readingVsIdealT)
			return "TURN_ON_HEATING"
		elif (self._read_temp > self._ideal_temp): 		#trigger high temp signal
			logger.debug("High temp signal " + readingVsIdealT)
			return "TURN_OFF_HEATING"
		else:											
			logger.debug("Perfect temp signal " + readingVsIdealT)
			return "TURN_OFF_HEATING"
		#if (self._read_humidity < self._ideal_humidity):
			#trigger low humidity signal
		#	logger.debug("Low humidity signal " + readingVsIdealH)
		#elif (self._read_humidity > self._ideal_humidity):
			#trigger high humidity signal
		#	logger.debug("High humidity signal " + readingVsIdealH)
		#else:
			#trigger perfect humidity signal
		#	logger.debug("Perfect humidity signal " + readingVsIdealH)

	def setupSchedulerTimeFromConfig(self):
		on = configHandler.getConfigHeatOnHour().split(".")
		off = configHandler.getConfigHeatOffHour().split(".")

		self._hour_on = int(on[0])		# Hour when heat should be turned on
		self._minute_on = int(on[1])	# Minute when heat should be turned on
		self._hour_off = int(off[0])	# Hour when heat should be turned off
		self._minute_off =int(off[1])	# Minute when heat should be turned off

	def IsValidHour(self, hour):
		if hour >= 0 and  hour <= 23:
			return True
		else:
			return False

	def IsValidMinute(self, minute):
		if minute >= 0 and  minute <= 59:
			return True
		else:
			return False

	def getComparableHourMinuteTimeStruct(self, hour, minute):
		dt = datetime.datetime.strptime(str(hour)+":"+str(minute), "%H:%M")
		return datetime.time(dt.hour, dt.minute)

	def doesSchedulerAllowsHeathing(self):
		if self._scheduler_control:
			dtimenow = datetime.datetime.now()
			now = self.getComparableHourMinuteTimeStruct(dtimenow.hour, dtimenow.minute)
			start = self.getComparableHourMinuteTimeStruct(self._hour_on, self._minute_on)
			stop = self.getComparableHourMinuteTimeStruct(self._hour_off, self._minute_off)
			
			if now >= start and now < stop:
				return True
			else:
				False
		else:
			return False

	def updateScheduler(self, hour_on, minute_on, hour_off, minute_off):
		self._hour_on = hour_on
		self._minute_on = minute_on
		self._hour_off = hour_off
		self._minute_off = minute_off
	
		configHandler.saveHeatOnHour(hour_on, minute_on)
		configHandler.saveHeatOffHour(hour_off, minute_off)

	def startTemperatureControl(self):
		self._air_control = True
		self._config.saveHeatControlState(1)
		logger.info("Temperature control is ON")

	def finishTemperatureControl(self):
		self._air_control = False
		self._config.saveHeatControlState(0)
		logger.info("Temperature control is OFF")

	def startScheduledHeating(self):
		self._scheduler_control = True
		self._config.saveSchedulerState(1)
		logger.info("Scheduled heating is ON")

	def stopScheduledHeating(self):
		self._scheduler_control = False
		self._config.saveSchedulerState(0)
		logger.info("Scheduled heating is OFF")
