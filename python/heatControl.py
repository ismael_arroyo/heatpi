#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import json
import webiopi
import time
import datetime

import configHandler
from configHandler import logger

GPIO = webiopi.GPIO 	# Helper for LOW/HIGH values
HEATING_PIN = 22			# GPIO pin where heat relay control is connected

	# Controller that allows to setup and modify a daily hh.mm when heat connected 
	# to HEATING_PIN sould be turned on and off. 
class HeatControl:
	def printValues(self):
		print ("*** Heat control values ***")
		print ("Main heat pin:", HEATING_PIN)
		print ("Main heat state:", self.isHeaterOn(HEATING_PIN))


	def isHeaterOn(self, pin = HEATING_PIN):
		return GPIO.digitalRead(pin) 

	def getHeaterStateJSON(self):
		return json.dumps({		
			"heat_state": self.isHeaterOn(HEATING_PIN)
		})

	def turnOnHeating(self):
		if not self.isHeaterOn(HEATING_PIN):
			GPIO.digitalWrite(HEATING_PIN, GPIO.HIGH)
			time.sleep(0.5)
			logger.info("Heating was turned ON!")

	def turnOffHeating(self):
		if self.isHeaterOn(HEATING_PIN):
			GPIO.digitalWrite(HEATING_PIN, GPIO.LOW)
			time.sleep(0.5)
			logger.info("Heating was turned OFF!")

	def __init__(self):
		GPIO.setFunction(HEATING_PIN, GPIO.OUT)		# set the GPIO used by the heat to output
		GPIO.digitalWrite(HEATING_PIN, GPIO.LOW)
		
		self._config = configHandler		# Instance to configuration services