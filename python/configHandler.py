#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import json
import os
import logging, logging.handlers
#import dropbox
import sys, time

from webiopi.utils.config import Config as webioPiConfig
from webiopi.server import getLocalIP

logger = logging.getLogger("HeatPi")
CONFIG_FILE = "heatpi_config.json"
_file_path = os.path.dirname(os.path.realpath(__file__)) +"/" + CONFIG_FILE


BACKUP_FREQ = 55		# Backup of configuration file frequency in MINUTES
AIR_CONTROL = 1		# When off heating stays always off ignoring reference temperature
TEMPERATURE = 23		# Temperature y CELSIUS degrees
HUMIDITY = 55			# Humidity in %
DRY_LIMIT = 40			# Soil humidity % limit to have an irrigation session
SCHEDULER = 0			# Scheduler
AIR_CTRL_TIME_ON = "6.15"	# Daily hour when the light is turned on
AIR_CTRL_TIME_OFF = "18.10" # Daily hour when the light is turned off 


__dropbox_token = None		# Token to upload/download config file with dropbox
__dropbox_client =None

def printValues():
	print ("*** Config values for heatpi ***")
	print ("Backup config freq. ", BACKUP_FREQ)
	print ("Dropbox token", __dropbox_token)
	print ("Temperature ", TEMPERATURE)
	print ("Humidity ", HUMIDITY)
	print ("Dry limit", DRY_LIMIT)
	print ("Turn on air control at ", AIR_CTRL_TIME_ON)
	print ("Turn off air control at ", AIR_CTRL_TIME_OFF)


def setIntervalBackupJob(hours):
	if(__dropbox_client is not None):
		Scheduler.Instance().addIntervalJob(uploadBackupToDropbox, "cfg_backup", hours=hours)

	
def getDropboxClientForheatpi():
	global __dropbox_client
	if(__dropbox_token is not None and len(__dropbox_token) > 0):
		try:
			__dropbox_client = None #dropbox.client.DropboxClient(__dropbox_token)
			logger.debug("Linked to dropbox with access token: %s" % __dropbox_token)
		except Exception as e:
			logger.error("Unable to link heatpi with dropbox account for automatic configuration backups. "+str(e))
	else:
		logger.warning("Dropbox token is not valid. Will not do "+ CONFIG_FILE + "backups.")


def getTailOfLogFileJSON():
	with open("/var/log/webiopi", "r") as logfile:
		lines = logfile.readlines()
	return json.dumps(lines, sort_keys=True, indent=4, separators=(',', ': '))
	
def setGlobalsFromConfig(heatpicfg):
	global BACKUP_FREQ, __dropbox_token, TEMPERATURE, HUMIDITY, DRY_LIMIT, AIR_CTRL_TIME_ON, AIR_CTRL_TIME_OFF
	BACKUP_FREQ = heatpicfg["backup_freq"]
	__dropbox_token = heatpicfg["dropbox_token"]
	AIR_CONTROL = heatpicfg["air_control_allowed"]
	TEMPERATURE = heatpicfg["temperature"]
	HUMIDITY = heatpicfg["humidity"]
	DRY_LIMIT = heatpicfg["dry_limit"]
	SCHEDULER = heatpicfg["scheduler"]
	AIR_CTRL_TIME_ON = heatpicfg["air_ctrl_time_on"]
	AIR_CTRL_TIME_OFF = heatpicfg["air_ctrl_time_off"]

def downloadBackupAndApplyConfig ():
	if(__dropbox_client is not None):
		backup_file, metadata = __dropbox_client.get_file_and_metadata('/' + CONFIG_FILE)
		local_file = open(_file_path, 'wb')
		local_file.write(backup_file.read())
		local_file.close()

		updateConfigFromFile()
		logger.info("Config backup sucesfully downloaded from dropbox.")

def start():
	updateConfigFromFile()			# So we can check the dropbox and pushbullet tokens
	
	getDropboxClientForheatpi()	# Attemps to set a connection with dropbox
	downloadBackupAndApplyConfig()

	setIntervalBackupJob(int(BACKUP_FREQ))

def updateConfigFromFile():
	with open(_file_path, "r") as configfile:
		heatpicfg = json.load(configfile)
		#print (str(heatpicfg))
	setGlobalsFromConfig(heatpicfg)

def uploadBackupToDropbox():
	if(__dropbox_client is not None):
		local_file = open(_file_path, 'r')
		try:
			response = __dropbox_client.put_file(CONFIG_FILE, local_file, overwrite=True)
			logger.info("Config backup sucesfully uploaded.")
		except Exception as e:
			logger.warning("Unable to upload config backup to dropbox."+str(e))

def getConfigDict():
	global BACKUP_FREQ, __dropbox_token, AIR_CONTROL, TEMPERATURE, HUMIDITY, DRY_LIMIT, SCHEDULER, AIR_CTRL_TIME_ON, AIR_CTRL_TIME_OFF
	return ({
		"backup_freq": BACKUP_FREQ,
		"dropbox_token": __dropbox_token,
		"air_control_allowed": AIR_CONTROL,
		"temperature" : TEMPERATURE,
		"humidity": HUMIDITY,
		"dry_limit": DRY_LIMIT,
		"scheduler": SCHEDULER,
		"air_ctrl_time_on": AIR_CTRL_TIME_ON,
		"air_ctrl_time_off": AIR_CTRL_TIME_OFF
	})

def saveConfigToFile():
	cfg = getConfigDict()
	with open(_file_path, "w") as configfile:
		logger.debug("Saving HeatPi cfg"+str(cfg))
		json.dump(cfg, configfile, sort_keys=True, indent=4, separators=(',', ': '))

def saveHeatControlState(state):
	global AIR_CONTROL
	if (state == 0 or state == 1):
		AIR_CONTROL = state
		saveConfigToFile()

def saveTemperature(celsius):
	global TEMPERATURE
	TEMPERATURE = celsius
	saveConfigToFile()

def saveHumidity(percent):
	global HUMIDITY
	HUMIDITY = percent
	saveConfigToFile()

def saveDryLimit(percent):
	global DRY_LIMIT
	DRY_LIMIT = percent
	saveConfigToFile()

def saveHeatOnHour(hour, minute):
	global AIR_CTRL_TIME_ON
	AIR_CTRL_TIME_ON = str(hour) + "." + str(minute)
	saveConfigToFile()

def saveHeatOffHour(hour, minute):
	global AIR_CTRL_TIME_OFF
	AIR_CTRL_TIME_OFF = str(hour) + "." + str(minute)
	saveConfigToFile()

def saveSchedulerState(state):
	global SCHEDULER
	if (state == 0 or state == 1):
		SCHEDULER = state
		saveConfigToFile()

def getConfigHeatingControl():
	return AIR_CONTROL

def getConfigTemperature():
	return TEMPERATURE

def getConfigHumidity():
	return HUMIDITY

def getConfigSchedulerState():
	return SCHEDULER

def getConfigHeatOnHour():
	return AIR_CTRL_TIME_ON

def getConfigHeatOffHour():
	return AIR_CTRL_TIME_OFF 

def getDryLimit():
	return DRY_LIMIT

