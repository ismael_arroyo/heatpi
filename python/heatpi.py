#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import webiopi
import time

####### Setup heatpi project path modules #######
import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

### Import heatpimodules ###
from airControl import AirControl
from heatControl import HeatControl
import configHandler
import pingAir
from configHandler import logger

HEAT_CONTROL = None
AIR_CONTROL = None

# setup function is automatically called at WebIOPi startup
def setup():
    global AIR_CONTROL, HEAT_CONTROL
    configHandler.start()
    #pingAir.start()

    AIR_CONTROL = AirControl()
    HEAT_CONTROL = HeatControl()
    logger.warning(">>>  Heatpi is working  <<<")

# loop function is repeatedly called by WebIOPi 
def loop():
    action = AIR_CONTROL.watchConditions()
    if action == "TURN_ON_HEATING":
        HEAT_CONTROL.turnOnHeating()
    else:
        HEAT_CONTROL.turnOffHeating()
    time.sleep(2) # 2 mins = 120

# destroy function is called at WebIOPi shutdown
def destroy():
    HEAT_CONTROL.turnOffHeating()
    configHandler.saveConfigToFile()
    logger.warning("<<<  Heatpi was shut down  >>>")

@webiopi.macro
def getStatus():
    return AIR_CONTROL.getAirControlStatusJSON()

@webiopi.macro
def getTailOfLogFile():
    return configHandler.getTailOfLogFileJSON()

@webiopi.macro
def getHeaterState():
    return HEAT_CONTROL.getHeaterStateJSON()

@webiopi.macro
def switchTemperatureControlTo(state):
    if (state == "True"):
        AIR_CONTROL.startTemperatureControl()
    else:
        AIR_CONTROL.finishTemperatureControl()
    return AIR_CONTROL.getAirControlStatusJSON()

@webiopi.macro
def switchSchedulerControlTo(state):
    if (state == "True"):
        AIR_CONTROL.startScheduledHeating()
    else:
        AIR_CONTROL.stopScheduledHeating()
    return AIR_CONTROL.getAirControlStatusJSON()

@webiopi.macro
def updateTemperature(celsius):
    celsius = int(celsius)
    if celsius >= 10 and celsius <= 30:
        return AIR_CONTROL.setIdealTemperature(celsius)
    else:
        return AIR_CONTROL.getAirControlStatusJSON()

@webiopi.macro
def updateHumidity(percentage):
    percentage = int(percentage)
    if percentage >= 0 and percentage <= 100:
        return AIR_CONTROL.setIdealHumidity(percentage)
    else:
        return AIR_CONTROL.getAirControlStatusJSON()
