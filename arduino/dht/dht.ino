#include <DHT.h>

#define DHTPIN 2
#define DHTTYPE DHT22

DHT dht(DHTPIN, DHTTYPE);

String readString;

void setup(void) {
  Serial.begin(9600);
  dht.begin();
}
 
void loop(void) {

  while (Serial.available())
  {
    delay(1); //delay to allow byte to arrive in input buffer
    char c = Serial.read();
    readString += c;
    readString.toLowerCase();
    readString.trim();
  }

  if (readString == "dhttemp" || readString == "dhttempc")
  {
    int new_t = dht.readTemperature();
    if (isnan(new_t)) {
      Serial.println(" - Failed to read from DHT");
    } else {
      t = new_t;
      Serial.println(t);
    }
  }
  else if (readString == "hum" || readString == "humid" || readString == "dhthum")
  {
    int new_h = dht.readHumidity();
    if (isnan(new_h)) {
      Serial.println(" - Failed to read from DHT");
    } else {
      h = new_h;
      Serial.println(h);
    }
  }  
  readString = "";
}
