function bindDeleteJobModal(rowbutton){
    var job = rowbutton.data('job'); // Extract info from data-job attribute
    $('#dynModal').data('rowid', rowbutton.data('rowid')) //Save 
    var modal = $('#dynModal');
    modal.find('.modal-title').text("Remove job?");
    modal.find('.modal-body').html("<b>" + job + "</b> will be removed from scheduler and not be executed again. Keep in mind the possible consecuences for the life under Growpi control. If you are sure  to continue then click 'Remove' if not click 'Cancel'");
    hideButtonNext();
    $('#btnYes').html("Remove")
    $('#btnYes').show();
    $('#btnNo').html("Cancel")
    $('#btnYes').bind("click", function(e) {
            e.preventDefault();
            webiopi().callMacro("removeJob", [job], showScheduledEvents);
            //$("[data-rowid='" + $('#dynModal').data('rowid') + "']").parent().parent().remove(); //Removal of table row
    });
}

function getJobInfo(response, jobId){
    var jobs = jQuery.parseJSON(response); 
    var jobInfo =[];
    for (var i = 0; i < jobs.length; i++) {
        if (jobs[i].id == jobId){
            jobInfo[0] = jobs[i].name;
            var trigger = jobs[i].trigger;
            trigger = trigger.split("["); //parse trigger string to object
            var triggerType = trigger[0];
            trigger = trigger[1].split("]");
            jobInfo[1] = triggerType;
            jobInfo[2] = trigger[0];
        }
    }
    return jobInfo;
}

function getTriggerValuesFromInput(trigger){
    if (trigger == "interval"){
        return [parseInt($("#hoursupdate").val()) + (parseInt($("#daysupdate").val()) * 24), parseInt($("#minutesupdate").val()), parseInt($("#secondsupdate").val())];

    }
    else if (trigger == "cron") {
        return [parseInt($("#hourupdate").val()), parseInt($("#minuteupdate").val()), 0];
    }
    return [];
}

function getCronInputs(cronValues){
    inputhours= "<input class='inputdigits' id='hourupdate' type='number' min='0' max='23' step='1' required='true' pattern='[0-9]*' value = '"+cronValues[0]+"'/>";
    inputminutes = "<input class='inputdigits' id='minuteupdate' type='number' min='0' max='59' step='1' required='true' pattern='[0-9]*' value = '"+cronValues[1]+"'/> <br>";
    return (inputhours + inputminutes + getTimeReference()); 
}

function getIntervalInputs(intervalValues){
    var inputdays= "<input class='inputdigits' id='daysupdate' type='number' min='0' step='1' required='true' pattern='[0-9]*' value = '"+intervalValues[0]+"'/> <label for='daysupdate'>days</label> <br>";
    var inputhours= "<input class='inputdigits' id='hoursupdate' type='number' min='0' max='23' step='1' required='true' pattern='[0-9]*' value = '"+intervalValues[1]+"'/> <label for='hoursupdate'>hours</label> <br>";
    var inputminutes = "<input class='inputdigits' id='minutesupdate' type='number' min='0' max='59' step='1' required='true' pattern='[0-9]*' value = '"+intervalValues[2]+"'/> <label for='minutesupdate'>minutes</label> <br>";
    var inputseconds = "<input class='inputdigits' id='secondsupdate' type='number' min='0' max='59' step='1' required='true' pattern='[0-9]*' value = '"+intervalValues[3]+"'/> <label for='secondsupdate'>seconds</label> <br>";
    var intervalAdvice = "<em>The job will be run for first time after the new interval that you have specified, then it will run again successively after the time interval passes.</em>"
    return (inputdays + inputhours + inputminutes + inputseconds + intervalAdvice);
}

function bindUpdateCronJob(jobId, trigger){
    trigger = trigger.replace(/=/g, ':');
    var cron = JSON.parse(JSON.stringify(eval("({" + trigger + "})")));
    $('#btnYes').bind("click", function(e) {
        e.preventDefault();
        webiopi().callMacro("rescheduleCronJob", [jobId, $("#hourupdate").val(), $("#minuteupdate").val()], showScheduledEvents);
    });
    $('#dynModal').find('.modal-body').html("Set new daily UTC* hour when <b>" + jobId + "</b> will be run:<br>"+ getCronInputs([cron.hour, cron.minute]));
}

function bindUpdateIntervalJob(jobId, trigger){
    var interval = trigger.split(':');
    var days = 0;
    if(trigger.indexOf("day") > -1){
        interval = trigger.split(',');
        days = interval[0].split(' ');
        days = parseInt(days[0].trim());
        interval = interval[1].split(':');
    }
    var hours = parseInt(interval[0].trim());
    var minutes = parseInt(interval[1].trim());
    var seconds = parseInt(interval[2].trim());
    $('#btnYes').bind("click", function(e) {
        e.preventDefault();
        totalhours = parseInt($("#hoursupdate").val()) + (parseInt($("#daysupdate").val()) * 24);
        webiopi().callMacro("rescheduleIntervalJob", [jobId, totalhours, $("#minutesupdate").val(), $("#secondsupdate").val()], showScheduledEvents);
    });
    $('#dynModal').find('.modal-body').html("Set new interval after <b>" + jobId + "</b> will be run:<br>"+ getIntervalInputs([days, hours, minutes, seconds]));
}

function bindUpdateJobModal(rowbutton){
    var jobId = rowbutton.data('job'); // Extract info from data-job attribute
    var modalTitle = "";
    var modalBody = "";
    $.post(w().context + "macros/getNextScheduledEvents/", function(response){ //Get jobs info against we attempt to update over the most recent data
        var jobInfo = getJobInfo(response, jobId);
        var jobName = jobInfo[0];
        var triggerType = jobInfo[1];
        var trigger = jobInfo[2];
        if(triggerType.indexOf("cron") > -1){
            bindUpdateCronJob(jobId, trigger);
        }
        else if(triggerType.indexOf("interval") > -1){
            bindUpdateIntervalJob(jobId, trigger); 
        }
        $('#dynModal').find('.modal-title').text("Update "+ triggerType +" job");
        hideButtonNext();
        $('#btnYes').html("Update");
        $('#btnYes').show();
        $('#btnNo').html("Cancel");
    });
}
function bindPauseJobModal(rowbutton){
    var jobId = rowbutton.data('job'); 
    var modal = $('#dynModal');
    modal.find('.modal-title').text("Pause job?");
    modal.find('.modal-body').html("Do you want to pause <b>" + jobId + "</b> job?");
    $('#btnYes').html("Pause");
    $('#btnNo').html("Cancel");
    $('#btnYes').bind("click", function(e) {
        e.preventDefault();
        webiopi().callMacro("pauseJob", [jobId], showScheduledEvents);
    });
}

function bindResumeJobModal(rowbutton){
    var jobId = rowbutton.data('job'); 
    var modal = $('#dynModal');
    modal.find('.modal-title').text("Resume job?");
    modal.find('.modal-body').html("Do you want to resume <b>" + jobId + "</b> job?");
    $('#btnYes').html("Resume");
    $('#btnNo').html("Cancel");    
    $('#btnYes').bind("click", function(e) {
        e.preventDefault();
        webiopi().callMacro("resumeJob", [jobId], showScheduledEvents);
    });
}

function validateCronInputs(){
    var hour = parseInt($("#hourupdate").val().trim());
    var minute = parseInt($("#minuteupdate").val().trim());
    if ((hour < 0 && hour > 23) || (minute < 0 && minute > 59) ){
        return false;
    }
    else{
        return true;
    }
}

function validateIntervalInputs(){
    var days = parseInt($("#daysupdate").val().trim());
    var hour = parseInt($("#hoursupdate").val().trim());
    var minute = parseInt($("#minutesupdate").val().trim());
    var second = parseInt($("#secondsupdate").val().trim());
    if ((days > 0) || (hour > 0 && hour < 24) || (minute > 0 && minute < 60) || (second > 0 && second < 60)){
        return true;
    }
    else{
        return false;
    }
}
function hideButtonNext(){
    $('#btnNext').unbind();
    $('#btnNext').hide();
}
function showTriggerSetupInModalBody(){
    var triggerInput = undefined;
    hideButtonNext();
    if (triggerType=="cron"){
        triggerInput = "Set daily hour when new job shoud be run:<br>"+ getCronInputs([0,0]);
    }
    else if (triggerType=="interval") {
        triggerInput = "Set duration of the interval between every job run: <br>" + getIntervalInputs([0,0,0,0]);
    }
    $('.modal-title').text("New job, step 4: setup the trigger");
    $('.modal-body').html(triggerInput);
    $('#btnYes').html("Create Job");
    $('#btnYes').show();
    $('#btnYes').bind("click", function(e) {
        var validation = false;
        if (triggerType=="cron"){
            validation = validateCronInputs();
        }
        else if (triggerType=="interval") {
            validation = validateIntervalInputs();
        }
        if (validation){
            var newJobParams =  [$('.modal-title').data("func"), $('.modal-title').data("jobId"),$('.modal-title').data("triggerType")];
            newJobParams = newJobParams.concat(getTriggerValuesFromInput(triggerType));
            webiopi().callMacro("addJob", newJobParams, showScheduledEvents);
           $('.modal-title').removeData();
        }
        else{
            e.preventDefault();
            e.stopImmediatePropagation();
            if($('#badtrigger').length == 0){
               var errorBanner = "<div class='alert alert-danger' id=badtrigger role='alert'>"+
                "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>"+
                "<span class='sr-only'>Error:</span>Enter a valid trigger setup</div>";
                $('.modal-body').prepend(errorBanner);
            }
        }
        
    });
}

function showTriggerSelectionInModalBody(){
    $('#btnNext').unbind();
    $('.modal-title').text("New job, step 3: select trigger type");
    var triggerSelect = "Select type of trigger:<br>"+
    "<input type='radio' id='interval' name='trigger' value='interval'> Interval (will run the job after the interval passes indefinitely) <br>"+
    "<input type='radio' id='cron' name='trigger' value='cron'> Cron (job will be run every day at the specified hour)<br>";
    $('.modal-body').html(triggerSelect);
    $('#btnNext').bind("click", function(e) {
        e.preventDefault();
        triggerType = $( "input:radio[name=trigger]:checked" ).val()
        if (triggerType != undefined){
            $('.modal-title').data("triggerType", triggerType);
            showTriggerSetupInModalBody();
        }
    });
}

function showSetNewJobIdInModalBody(){
    $('#btnNext').unbind();
    $('.modal-title').text("New job, step 2: give it a name");
    $('.modal-body').html("Give an unique name to the new job. It can't be equal to any of the current jobs IDs:<br>");
    $('.modal-body').append("Job ID: <input type='text' id='jobId'>");
    $('#btnNext').bind("click", function(e) {
        e.preventDefault();
        jobId = $("#jobId" ).val().trim();
        if (jobId != "" && $('.modal-title').data("currentJobs").indexOf(jobId) == -1 ){
            $('.modal-title').data("jobId", jobId);
            showTriggerSelectionInModalBody();
        }
    });
}

function showFunctSelectionInModalBody(){
    $('.modal-title').text("New job, step 1: select function to be run");
    $('.modal-body').html("Select function to be run based on previous jobs:<br>");
    $.post(w().context + "macros/getRegisteredJobs/", function(response){ //Get jobIds that will be used as base to create other jobs calling the same function
        var jobs = jQuery.parseJSON(response);
        for (var i = 0; i < jobs.length; i++) {
            $('.modal-body').append("<input type='radio' name='func' value='"+jobs[i]+"'>"+jobs[i]+"<br>");
        }
        $('.modal-body').append("<br>");
        $('.modal-title').data("currentJobs", jobs);
    });
    $('#btnNext').bind("click", function(e) {
        e.preventDefault();
        selectedFunction = $("input:radio[name=func]:checked").val();
        if (selectedFunction != undefined){
            $('.modal-title').data("func", selectedFunction);
            showSetNewJobIdInModalBody();
        }
    });
}

function bindAddJobModal(rowbutton){
    var modal = $('#dynModal');
    $('#btnYes').hide();
    $('#btnNext').show();
    showFunctSelectionInModalBody();
}

function bindShowModalEvent(event){
    var rowbutton = $(event.relatedTarget); // Button that triggered the modal
    var context = rowbutton.data('context');
    $('.modal-body').focus();
    switch(context){
        case "removeJob":
            bindDeleteJobModal(rowbutton);
            break;
        case "updateJob":
            bindUpdateJobModal(rowbutton);
            break;
        case "pauseJob":
            bindPauseJobModal(rowbutton);
            break;
        case "resumeJob":
            bindResumeJobModal(rowbutton);
            break;
        case "addJob":
            bindAddJobModal(rowbutton);
            break;
    }
}
function unbindShowModalEvent(event){
    $('#btnYes').unbind();
    $('#btnNext').unbind();
}