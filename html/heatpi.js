
function bindUpdateControlsWithMacros(){
    $("#btnStart").click(function() {
        webiopi().callMacro("switchTemperatureControlTo", ["True"], showUiForStatus);
    });
    $("#btnStop").click(function() {
        webiopi().callMacro("switchTemperatureControlTo", ["False"], showUiForStatus);  
    });

    $("#btnSchedStart").click(function() {
        webiopi().callMacro("switchSchedulerControlTo", ["True"],showUiForStatus);
    });

    $("#btnSchedStop").click(function() {
        webiopi().callMacro("switchSchedulerControlTo", ["False"], showUiForStatus);  
    });

    $("#minusBtn").click(function() {
        var tempRef = $("#tempPerfect").text();
        if (tempRef !== "-"){
            webiopi().callMacro("updateTemperature", [parseInt(tempRef)-1], showUiForStatus);
        }
    });
    $("#plusBtn").click(function() {
        var tempRef = $("#tempPerfect").text();
        if (tempRef !== "-"){
            webiopi().callMacro("updateTemperature", [parseInt(tempRef)+1], showUiForStatus);
        }
    });
}

var showUiForStatus = function(macro, args, response) {
    var airParams = jQuery.parseJSON(response); //JSON {temperaure in ºC, humidity in %}
    $("#temperature, #tempPerfect, #humidity,#humidityperfect").empty()
    $("#temperature").append(Math.round(airParams.temperature)+ " &#176;C");
    $("#tempPerfect").text(Math.round(airParams.perfect_temperature));
    $("#tempupdate").val(airParams.perfect_temperature);
    $("#humidity").append(airParams.humidity+ " &#37;");
    $("#humidityperfect").append(airParams.perfect_humidity+ " &#37;");
    $("#nextSwitch").empty()
    $("#nextSwitch").append(airParams.air_ctrl_time_on + " - " +airParams.air_ctrl_time_off);

    if (airParams.air_control){
        $("#btnStart").addClass("btn-primary");
        $("#btnStart").removeClass("btn-default");
        $("#minusBtn").prop('disabled', false);
        $("#plusBtn").prop('disabled', false);
        $(".perfct-temp").removeClass('disabled');
        $("#rowScheduler").show();
    }
    else{
        $("#btnStart").addClass("btn-default");
        $("#btnStart").removeClass("btn-primary");
        $("#minusBtn").prop('disabled', true);
        $("#plusBtn").prop('disabled', true);
        $(".perfct-temp").addClass('disabled');
        $("#nextSwitch").addClass('disabled');
        $("#rowScheduler").hide();

    }
    if (airParams.scheduler_control){
        $("#btnSchedStart").addClass("btn-primary");
        $("#btnSchedStart").removeClass("btn-default");
        $("#nextSwitch").removeClass('disabled');
    }
    else{
        $("#btnSchedStart").addClass("btn-default");
        $("#btnSchedStart").removeClass("btn-primary");
        $("#nextSwitch").addClass('disabled');
    }
}

var showTailOfLogFile = function(macro, args, response) {
    var logLines = jQuery.parseJSON(response); 
    $("#logEvents").html("");
    for (index = 0; index < logLines.length; ++index) {
        line = logLines[index].replace(/\r\n/g, '\n');
        if (/WARNING/i.test(line) || /ERROR/i.test(line) || /CRITICAL/i.test(line)){
            $("#logEvents").html($("#logEvents").html() + "<span class='logerror'>" + line + "</span>"); 
        }
        else{
           $("#logEvents").html($("#logEvents").html() + line); 
       }
   }
   $("#logEvents").scrollTop($("#logEvents")[0].scrollHeight);
}

var showScheduledEvents = function(macro, args, response) {
    var jobs = jQuery.parseJSON(response); 
    $("#taskTable tbody").empty();
    $("#taskTable").append("<tbody></tbody>");
    for (var i = 0; i < jobs.length; i++) {
        var row = $("<tr/>")
        $("#taskTable tbody").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
        row.append($("<td>" + jobs[i].id + "</td>"));
        row.append($("<td>" + jobs[i].next_run_time + "</td>"));
        row.append($("<td>" + jobs[i].trigger + "</td>"));
        row.append($("<td>" + jobs[i].name + "</td>"));
        row.append($("<td><a href='#dynModal' class='job-button' role='button' data-toggle='modal' data-context='updateJob' data-macro='updateJob' data-job='"+jobs[i].id+"' data-rowid='"+i+"'><span class='glyphicon glyphicon-pencil'></span></a></td>"));
        row.append($("<td><a href='#dynModal' class='job-button' role='button' data-toggle='modal' data-context='removeJob' data-macro='removeJob' data-job='"+jobs[i].id+"' data-rowid='"+i+"'><span class='glyphicon glyphicon-remove'></span></a></td>"));
        if (jobs[i].next_run_time == "paused"){
            row.append($("<td><a href='#dynModal' class='job-button' role='button' data-toggle='modal' data-context='resumeJob' data-macro='resumeJob' data-job='"+jobs[i].id+"' data-rowid='"+i+"'><span class='glyphicon glyphicon-play'></span></a></td>"));
        }
        else{
            row.append($("<td><a href='#dynModal' class='job-button' role='button' data-toggle='modal' data-context='pauseJob' data-macro='pauseJob' data-job='"+jobs[i].id+"' data-rowid='"+i+"'><span class='glyphicon glyphicon-pause'></span></a></td>"));
        }
    }
    //Show a reference for better understanding UTC time VS local time
    $("#timezone_offset").html(getTimeReference());
}

function getTimeReference() {
    var d = new Date();
    var str = "<em data-toggle='tooltip' title='The cron trigger works with " +
    "the so-called “wall clock” time. Thus, if your current time zone observes " + 
    "DST (daylight saving time), you should be aware that it may cause unexpected " +
    "behavior with the cron trigger when entering or leaving DST. When switching " +
    "from standard time to daylight saving time, clocks are moved either " +
    "one hour or half an hour forward, depending on the time zone. For that reason " +
    "we have choosen to work only with UTC time which is not affected by DST'>" +
    "<b>*UTC</b> = local time";
    var difference = new Date().getTimezoneOffset()/60;
    if (difference < 0) {
        str = str + " - " + difference *(-1) + "h</em>";
    }
    else if (difference > 0){
        str = str + " + " + difference *(-1) + "h</em>";
    }
    else{
        str = str + "</em>";
    }
    var localHour = d.toLocaleTimeString().split(":", 2);
    var utcHour = addLeftZeroWhenLenghIsOne(d.getUTCHours().toString())+":"+addLeftZeroWhenLenghIsOne(d.getUTCMinutes().toString())
    return str + "<em> <b>e.g.</b> your local time is: "+ localHour[0]+":"+localHour[1] + " = " + utcHour + " UTC</em>";
}
function addLeftZeroWhenLenghIsOne(digit){
    if (digit.length == 1) return "0" + digit;
    else return digit;
}
